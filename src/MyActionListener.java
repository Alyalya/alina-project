import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener
{
    public void actionPerformed(ActionEvent e)
    {
        Exam.exam(Exam.group);
        JList groupAfterExam = new JList(Exam.exam(Exam.group).toArray());

        Box boxAfterExamGroup = Box.createHorizontalBox();
        boxAfterExamGroup.add(new JLabel("Список студентов после экзамена:"));
        boxAfterExamGroup.add(groupAfterExam);
        boxAfterExamGroup.add(Box.createHorizontalGlue());
        boxAfterExamGroup.add(Box.createHorizontalStrut(12));

        Exam.myWindow.setContentPane(boxAfterExamGroup);
        Exam.myWindow.pack();
        Exam.myWindow.setVisible(true);
    }
}
