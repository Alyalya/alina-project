import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;


public class Exam {

  public static List<String> group = new ArrayList<>();
  public static JFrame myWindow = new SimpleWindow("Список студентов");


  public static List<String> exam(List<String> group) {
    return group.stream().map(man -> {
      if (man.equals("Alina") || man.equals("Ivan")) {
        man += " получает 5 и успешно проходит экзамен";
      } else man += " идёт в армию";
      return man;
    }).collect(Collectors.toList());
  }

  /**
     * Some text here.
     */
  public static void main(String[] args)
            throws Exception {
    group.add("Alina");
    group.add("Ivan");
    group.add("Tanya");
    group.add("Vasily");

    JLabel groupLabel = new JLabel("Список студентов группы:");
    JList groupSimple = new JList(group.toArray());
    JButton examBtn = new JButton("Провести экзамен");
    ActionListener actionListener = new MyActionListener();
    examBtn.addActionListener(actionListener);

    Box boxSimpleGroup = Box.createHorizontalBox();

    boxSimpleGroup.add(groupLabel);
    boxSimpleGroup.add(groupSimple);
    boxSimpleGroup.add(Box.createHorizontalGlue());
    boxSimpleGroup.add(Box.createHorizontalStrut(12));
    boxSimpleGroup.add(examBtn);

    myWindow.setContentPane(boxSimpleGroup);
    myWindow.pack();
    myWindow.setVisible(true);
  }

}